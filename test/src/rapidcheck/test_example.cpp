#include <header_only/headeronly.h>

#include <gtest/gtest.h>
#include <rapidcheck/gtest.h>

RC_GTEST_PROP(MyTestCase,
              copyOfStringIsIdenticalToOriginal,
              ())
{
    RC_ASSERT(foo() == "foo");
}

int main(int argc, char **argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
