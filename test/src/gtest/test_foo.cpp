#include <header_only/headeronly.h>

#include <gtest/gtest.h>
#include <iostream>

TEST(FooTest, FooStrIsReturned)
{
    ASSERT_STREQ(foo(), "foo");
}
