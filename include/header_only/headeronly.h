#ifndef HEADERONLY_H
#define HEADERONLY_H

#include <cstring>

#ifdef __cplusplus
extern "C" {
#endif

inline const char* foo();

inline const char* foo()
{
    return "foo";
}

#ifdef __cplusplus
} // extern "C"
#endif

#endif // HEADERONLY_H
