from conan import ConanFile
from conan.tools.cmake import CMakeToolchain, CMake, CMakeDeps, cmake_layout


class ConanPackage(ConanFile):
    name = "header-only-library"
    version = "0.0.1"
    settings = "os", "arch", "compiler", "build_type"
    exports_sources = "CMakeLists.txt", "include/*", "test/*"
    no_copy_source = True

    def configure(self):
        self.options["rapidcheck/*"].enable_gtest = True

    def build_requirements(self):
        self.tool_requires("cmake/3.26.3")
        self.test_requires("gtest/1.12.1")
        self.test_requires("rapidcheck/cci.20230815")

    def layout(self):
        cmake_layout(self)

    def generate(self):
        tc = CMakeToolchain(self)
        tc.generate()
        deps = CMakeDeps(self)
        deps.generate()

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()
        cmake.install()
        cmake.test()

    def package(self):
        cmake = CMake(self)
        cmake.install()

    def package_info(self):
        self.cpp_info.bindirs = []
        self.cpp_info.libdirs = []

        if self.settings.os in ["iOS", "Macos"]:
            self.cpp_info.frameworks.extend(["Foundation"])
