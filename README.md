# Conan example of a header-only library

## Build:

Use `conan`:

```shell
conan build -of build .
```

You can also create a package:

```shell
conan create .
```
